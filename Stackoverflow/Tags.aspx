﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Stackoverflow.Master" AutoEventWireup="true" CodeBehind="Tags.aspx.cs" Inherits="Stackoverflow.Tags" %>
<asp:Content ID="TagsHead" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <link href="Resources/Styles/tags-style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="TagsContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="TagsPage" class="tag-wrapper" runat="server">
        <!-- Dynamic content -->
    </div>
</asp:Content>
