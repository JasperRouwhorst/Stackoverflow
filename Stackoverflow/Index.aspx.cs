﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Security;

namespace Stackoverflow
{
    public partial class Index : System.Web.UI.Page
    {
        private WindowHandler windowHandler;

        private List<Question> questions;


        protected void Page_Load(object sender, EventArgs e)
        {
            windowHandler = WindowHandler.GetInstance();

            questions = DatabaseHandler.GetInstance().DownloadQuestions();

            foreach (Question question in questions)
            {
                questionList.Controls.Add(windowHandler.DrawQuestionListItem(question));
            }
            
        }
    }
}