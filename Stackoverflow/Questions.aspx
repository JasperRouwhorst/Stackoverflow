﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Stackoverflow.Master" AutoEventWireup="true" CodeBehind="Questions.aspx.cs" Inherits="Stackoverflow.Questions" %>
<asp:Content ID="QuestionsHead" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <link href="Resources/Styles/questions-style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="QuestionsContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <ul id="questionList" runat="server">
            <!-- Dynamic content --> 
    </ul>
</asp:Content>
