﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Security.Principal;

namespace Stackoverflow
{
    public partial class Stackoverflow : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] == null)
            {
                string name = HttpContext.Current.User.Identity.Name;
                // Create session to create the user object
                Session["User"] = DatabaseHandler.GetInstance().DownloadUser(name);

            }
            // Set the dynamic Master variables
            User user = (User)Session["User"];
            headerReputation.InnerText = user.Reputation.ToString();
            headerBadges.InnerText = user.BadgeList.Count().ToString();

            topbarProfileButton.HRef = "ProfilePage.aspx?userid=" + user.ID;
        }

        protected void topbarInboxButton_Click(object sender, ImageClickEventArgs e)
        {

        }

        protected void topbarAchievementButton_Click(object sender, ImageClickEventArgs e)
        {

        }
    }
}