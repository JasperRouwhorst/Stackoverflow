﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Stackoverflow.Master" AutoEventWireup="true" CodeBehind="AskQuestion.aspx.cs" Inherits="Stackoverflow.AskQuestion" %>
<asp:Content ID="AskQuestionHead" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <link href="Resources/Styles/askquestion-style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="AskQuestionContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="AskQuestion">
        <div id="title-wrapper">
            <span>Title: </span>
            <asp:TextBox ID="titleTextBox" CssClass="title-textbox" runat="server" />
        </div>
        <div id="question-content-wrapper">
            <span>Question: </span>
           <asp:TextBox ID="questionContentTextBox" CssClass="question-content-textbox" TextMode="MultiLine" runat="server" />
        </div>
        <div id="tag-wrapper">
            <span>Tags: </span>
            <asp:TextBox ID="tagTextBox" CssClass="tag-textbox" runat="server" />
        </div>
        <div id="submit-wrapper">
            <asp:Button ID="questionSubmitButton" CssClass="submit-button" Text="Post your Question" OnClick="QuestionSubmit_Click" runat="server" />
        </div>
    </div>
</asp:Content>
