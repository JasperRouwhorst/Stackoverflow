﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Stackoverflow.Master" AutoEventWireup="true" CodeBehind="QuestionDetail.aspx.cs" Inherits="Stackoverflow.QuestionDetail" %>

<asp:Content ID="QuestionDetailHead" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <link href="Resources/Styles/questiondetail-style.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="QuestionDetailContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="QuestionDetail">
        <div id="question-container">
            <div id="titleWrapper" class="title-wrapper" runat="server">
                <!-- Dynamic content -->
            </div>
            <div class="content-container">

                <div class="vote-wrapper">
                    <div class="vote-button">
                        <asp:ImageButton ID="postVoteUpButton" runat="server" />
                    </div>
                    <div class="vote-button">
                        <asp:ImageButton ID="postFavoriteButton" runat="server" />
                    </div>
                    <div class="vote-button">
                        <asp:ImageButton ID="postVoteDownButton" runat="server" />
                    </div>
                </div>

                <div class="content-wrapper" >
                    <div id="contentWrapper" runat="server">
                        <!-- Dynamic content -->
                    </div>
                    <div id="tagWrapper" class="tag-wrapper" runat="server">
                        <!-- Dynamic content -->
                    </div>
                    <div id="authorWrapper" class="author-wrapper" runat="server">
                        <!-- Dynamic content -->
                    </div>
                </div>

            </div>        
        </div>
        <div class="divider">
            <h1>Answers</h1>
        </div>
        <div id="answerContainer" runat="server">
            <!-- Dynamic content -->
        </div>
        <div class="divider">
            <h2>Your answer</h2>
        </div>
        <div id="youranswer-container">
            <div id="youranswer-content-wrapper">
                <asp:TextBox ID="yourAnswerTextbox" TextMode="MultiLine" CssClass="youranswer-content-textbox" runat="server" />
            </div>
            <div id="submit-wrapper">
                <asp:Button ID="answerSubmitButton" CssClass="submit-button" Text="Post your Answer" OnClick="AnswerSubmit_Click" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
