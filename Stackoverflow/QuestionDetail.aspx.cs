﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Stackoverflow
{
    public partial class QuestionDetail : System.Web.UI.Page
    {

        private Question postQuestion;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["questionid"] != null){
                postQuestion = DatabaseHandler.GetInstance().DownloadQuestionByID(Convert.ToInt32(Request.QueryString["questionid"]));
            }
            else
            {
                Response.Redirect("404.html");
            }

            if (postQuestion.VotedBy(((User)Session["User"]).ID) == 1)
            {
                postVoteUpButton.Click += new ImageClickEventHandler(questionUnvoteUp_Click);
            }
            else
            {
                postVoteUpButton.Click += new ImageClickEventHandler(questionVoteUp_Click);
            }

            if (postQuestion.VotedBy(((User)Session["User"]).ID) == -1)
            {
                postVoteDownButton.Click += new ImageClickEventHandler(questionUnVoteDown_Click);
            }
            else
            {
                postVoteDownButton.Click += new ImageClickEventHandler(questionVoteDown_Click);
            }

            if (postQuestion.FavoritedBy(((User)Session["User"]).ID))
            {
                postFavoriteButton.Click += new ImageClickEventHandler(questionUnfavorite_Click);
            }
            else
            {
                postFavoriteButton.Click += new ImageClickEventHandler(questionFavorite_Click);
            }

            // Set favorited button
            postFavoriteButton.ImageUrl = postQuestion.FavoritedBy(((User)Session["User"]).ID) == true ? "Resources/Images/favorite-checked.png" : "Resources/Images/favorite-unchecked.png";
            postVoteUpButton.ImageUrl = postQuestion.VotedBy(((User)Session["User"]).ID) == 1 ? "Resources/Images/vote-up-checked.png" : "Resources/Images/vote-up-unchecked.png";
            postVoteDownButton.ImageUrl = postQuestion.VotedBy(((User)Session["User"]).ID) == -1 ? "Resources/Images/vote-down-checked.png" : "Resources/Images/vote-down-unchecked.png";
            
            // Set the title
            titleWrapper.InnerHtml = "<h1>" + postQuestion.Title + "</h1>";
            contentWrapper.InnerHtml = "<span>"+postQuestion.Content+"</span>";

            tagWrapper.InnerHtml = "";
            foreach (string tag in postQuestion.TagList)
            {
                tagWrapper.InnerHtml += "<span class=\"tag\">"+tag+"</span>";
            }

            authorWrapper.InnerHtml += "<span class=\"author\">"+postQuestion.GetAuthor().Username+"</span>";
            authorWrapper.InnerHtml += "<span class=\"date\">" + postQuestion.PostDate + "</span>";

            foreach (Answer answer in postQuestion.AnswerList)
            {
                answerContainer.Controls.Add(WindowHandler.GetInstance().DrawAnswerItem(answer));
            }

        }

        protected void questionVoteUp_Click(object sender, ImageClickEventArgs args)
        {
            int id = Convert.ToInt32(Request.QueryString["questionid"]);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().VoteQuestion(userid, id, 1);


            ImageButton button = ((ImageButton)sender);
            button.Click -= new ImageClickEventHandler(questionVoteUp_Click);
            button.Click += new ImageClickEventHandler(questionUnvoteUp_Click);

            button.ImageUrl = "Resources/Images/vote-up-checked.png";

        }

        protected void questionUnvoteUp_Click(object sender, ImageClickEventArgs args)
        {
            int id = Convert.ToInt32(Request.QueryString["questionid"]);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().UnvoteQuestion(userid, id, 1);


            ImageButton button = ((ImageButton)sender);
            button.Click -= new ImageClickEventHandler(questionVoteUp_Click);
            button.Click += new ImageClickEventHandler(questionUnvoteUp_Click);

            button.ImageUrl = "Resources/Images/vote-up-unchecked.png";

        }

        protected void questionFavorite_Click(object sender, ImageClickEventArgs args)
        {
            int id = Convert.ToInt32(Request.QueryString["questionid"]);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().FavoriteQuestion(id);

            ImageButton button = ((ImageButton)sender);
            button.Click -= new ImageClickEventHandler(questionFavorite_Click);
            button.Click += new ImageClickEventHandler(questionUnfavorite_Click);

            button.ImageUrl = "Resources/Images/favorite-checked.png";

        }

        protected void questionUnfavorite_Click(object sender, ImageClickEventArgs args)
        {
            int id = Convert.ToInt32(Request.QueryString["questionid"]);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().UnfavoriteQuestion(id);

            ImageButton button = ((ImageButton)sender);
            button.Click -= new ImageClickEventHandler(questionUnfavorite_Click);
            button.Click += new ImageClickEventHandler(questionFavorite_Click);

            button.ImageUrl = "Resources/Images/favorite-unchecked.png";

        }

        protected void questionVoteDown_Click(object sender, ImageClickEventArgs args)
        {
            int id = Convert.ToInt32(Request.QueryString["questionid"]);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().VoteQuestion(userid, id, -1);

            ImageButton button = ((ImageButton)sender);
            button.Click -= new ImageClickEventHandler(questionVoteDown_Click);
            button.Click += new ImageClickEventHandler(questionUnVoteDown_Click);

            button.ImageUrl = "Resources/Images/vote-down-checked.png";

        }

        protected void questionUnVoteDown_Click(object sender, ImageClickEventArgs args)
        {
            int id = Convert.ToInt32(Request.QueryString["questionid"]);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().UnvoteQuestion(userid, id, -1);

            ImageButton button = ((ImageButton)sender);
            button.Click -= new ImageClickEventHandler(questionUnVoteDown_Click);
            button.Click += new ImageClickEventHandler(questionVoteDown_Click);

            button.ImageUrl = "Resources/Images/vote-down-unchecked.png";

        }

        protected void AnswerSubmit_Click(object sender, EventArgs args)
        {
            int id = DatabaseHandler.GetInstance().GetNextID("SO_ANSWER", "ANSWERID");
            int userid = ((User)Session["User"]).ID;
            Answer answer = new Answer(id, userid, postQuestion, yourAnswerTextbox.Text, 0, 0, DateTime.Now.ToString());
            DatabaseHandler.GetInstance().UploadNewAnswer(answer, postQuestion);
            Response.Redirect(Request.RawUrl);
        }

    }
}