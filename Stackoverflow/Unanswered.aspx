﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Stackoverflow.Master" AutoEventWireup="true" CodeBehind="Unanswered.aspx.cs" Inherits="Stackoverflow.Unanswered" %>
<asp:Content ID="UnansweredHead" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <link href="Resources/Styles/questions-style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="UnansweredContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div id="UnansweredDiv" runat="server">
        <!-- Dynamic content -->
    </div>
</asp:Content>
