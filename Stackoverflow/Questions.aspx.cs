﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Stackoverflow
{
    public partial class Questions : System.Web.UI.Page
    {

        WindowHandler windowHandler;

        private List<Question> questions;

        protected void Page_Load(object sender, EventArgs e)
        {
            windowHandler = WindowHandler.GetInstance();

            if (Request.QueryString["tag"] != null)
            {
                questions = DatabaseHandler.GetInstance().DownloadQuestionsByTag(Request.QueryString["tag"]);
            }
            else
            {
                questions = DatabaseHandler.GetInstance().DownloadQuestions(0);

            }

            foreach (Question question in questions)
            {
                questionList.Controls.Add(windowHandler.DrawQuestionListItem(question));
            }

        }
    }
}