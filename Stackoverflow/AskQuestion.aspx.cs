﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Stackoverflow
{
    public partial class AskQuestion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void QuestionSubmit_Click(object sender, EventArgs e)
        {
            int id = DatabaseHandler.GetInstance().GetNextID("SO_QUESTION", "QUESTIONID");
            User user = (User)Session["User"];
            Question question = new Question(id, user.ID, titleTextBox.Text, questionContentTextBox.Text, 0, 0, 0, DateTime.Now.ToString());

            List<string> taglist = new List<string>();
            string tagstring = tagTextBox.Text;
            
            // Split the individual tags and add them to a list 
            while (tagstring.Length > 0)
            {
                int semicolon = tagstring.IndexOf(';');
                if (semicolon > 0)
                {
                    tagstring = tagstring.Replace(" ", String.Empty);
                    string tag = tagstring.Substring(0, semicolon);
                    tagstring = tagstring.Remove(0, semicolon+1);
                    taglist.Add(tag);

                }
            }

            // Add the taglist to the question
            question.TagList = taglist;
            
            DatabaseHandler.GetInstance().UploadNewQuestion(question);
            Response.Redirect("QuestionDetail.aspx?QuestionID=" + question.ID);
        }
    }
}