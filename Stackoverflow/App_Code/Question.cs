﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stackoverflow
{
    public class Question : Post
    {

        public string Title { get; set; }
        public int Favorites { get; set; }

        private List<string> taglist;
        private List<Answer> answerlist;
        private Boolean answered = new Boolean();

        public bool Answered
        {
            get
            {
                return DatabaseHandler.GetInstance().DownloadAnswersForQuestion(this).Count() > 0;
            }
            set
            {
                answered = value;
            }
        }

        public List<string> TagList
        {
            get
            {
                return taglist == null ? DatabaseHandler.GetInstance().DownloadTagsForQuestion(ID) : taglist;
            }
            set
            {
                taglist = value;
            }
        }

        public List<Answer> AnswerList
        {
            get
            {
                return answerlist == null ? DatabaseHandler.GetInstance().DownloadAnswersForQuestion(this) : answerlist;
            }
            set
            {
                answerlist = value;
            }
        }

        public Question(int id, int userid, string title, string content, int upvotes, int downvotes, int favorites, string postdate)
            : base(id, userid, content, upvotes, downvotes, postdate)
        {
            Title = title;
            Favorites = favorites;
        }

        public bool FavoritedBy(int userid)
        {
            return DatabaseHandler.GetInstance().CheckQuestionFavoritedByUser(ID, userid);
        }

        public int VotedBy(int userid)
        {
            return DatabaseHandler.GetInstance().CheckQuestionVoteByUser(ID, userid);
        }


    }
}