﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stackoverflow
{
    public class User
    {

        public readonly int ID;
        public string Username { get; set; }

        public int Reputation { get; set; }
        public string RealName { get; set; }
        public string Location { get; set; }
        public int Age { get; set; }
        public int MemberFor { get; set; }
        public int Visited { get; set; }
        public string LastSeen { get; set; }
        public int ProfileViews { get; set; }
        public string Email { get; set; }

        public List<string> BadgeList 
        {
            get
            {
                return DatabaseHandler.GetInstance().DownloadBadgesForUser(ID);
            }
            set
            {
                BadgeList = value;
            }
        }

        public User(int id, string username)
        {
            ID = id;
            Username = username;
        }

        public User(int id, string username, int reputation, int memberfor, int visited, string lastseen, int profileviews)
        {
            ID = id;
            Username = username;
            Reputation = reputation;
            MemberFor = memberfor;
            Visited = visited;
            LastSeen = lastseen;
            ProfileViews = profileviews;
        }

        
    }
}