﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stackoverflow
{
    public class Message
    {

        public readonly int ID;
        public readonly int InboxID;
        public readonly DateTime Date;
        public readonly String Content;

        public Message(int id, int inboxid, DateTime date, string content)
        {
            ID = id;
            InboxID = inboxid;
            Date = date;
            Content = content;
        }

    }
}