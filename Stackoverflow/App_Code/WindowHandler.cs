﻿using System;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace Stackoverflow
{
    public class WindowHandler
    {

        private static WindowHandler self;

        private WindowHandler()
        {
            self = this;
        }

        public static WindowHandler GetInstance()
        {
            if (self == null)
            {
                return new WindowHandler();
            }
            else
            {
                return self;
            }
        }

        public HtmlGenericControl DrawQuestionListItem(Question question)
        {
            HtmlGenericControl postcontainer = new HtmlGenericControl("div");
            HtmlGenericControl statscontainer = new HtmlGenericControl("div");
            HtmlGenericControl detailscontainer = new HtmlGenericControl("div");
            postcontainer.Attributes.Add("class", "post-listitem");
            statscontainer.Attributes.Add("class", "stats-container");
            detailscontainer.Attributes.Add("class", "details-container");
            
            HtmlGenericControl votewrapper = new HtmlGenericControl("div");
            votewrapper.Attributes.Add("class", "stat-wrapper");
            HtmlGenericControl voteamount = new HtmlGenericControl("span");
            voteamount.InnerText = (question.Upvotes - question.Downvotes).ToString();
            voteamount.Attributes.Add("class", "stat-value");
            HtmlGenericControl votetitle = new HtmlGenericControl("span");
            votetitle.Attributes.Add("class", "stat-title");
            votetitle.InnerText = "Votes";
            votewrapper.Controls.Add(voteamount);
            votewrapper.Controls.Add(votetitle);

            HtmlGenericControl answerwrapper = new HtmlGenericControl("div");
            answerwrapper.Attributes.Add("class", "stat-wrapper "+ (question.Answered ? "answered" : ""));
            HtmlGenericControl answeramount = new HtmlGenericControl("span");
            answeramount.InnerText = question.AnswerList.Count().ToString();
            answeramount.Attributes.Add("class", "stat-value");
            HtmlGenericControl answertitle = new HtmlGenericControl("span");
            answertitle.Attributes.Add("class", "stat-title");
            answertitle.InnerText = "Answers";
            answerwrapper.Controls.Add(answeramount);
            answerwrapper.Controls.Add(answertitle);

            HtmlGenericControl title = new HtmlGenericControl("h3");
            title.InnerText = question.Title;
            title.Attributes.Add("class", "title");
            HtmlGenericControl titlelink = new HtmlGenericControl("a");
            titlelink.Attributes.Add("href", "QuestionDetail.aspx?questionid=" + question.ID);
            titlelink.Controls.Add(title);

            HtmlGenericControl postedby = new HtmlGenericControl("span");
            postedby.Attributes.Add("class", "postedby");
            postedby.InnerText = question.GetAuthor().Username;

            postcontainer.Controls.Add(statscontainer);

            statscontainer.Controls.Add(votewrapper);
            statscontainer.Controls.Add(answerwrapper);
            postcontainer.Controls.Add(detailscontainer);
            detailscontainer.Controls.Add(titlelink);
            detailscontainer.Controls.Add(postedby);

            return postcontainer;
        }

        public HtmlGenericControl DrawAnswerItem(Answer answer)
        {
            HtmlGenericControl answercontainer = new HtmlGenericControl("div");
            answercontainer.Attributes.Add("class", "content-container");

                HtmlGenericControl votewrapper = new HtmlGenericControl("div");
                votewrapper.Attributes.Add("class", "vote-wrapper");

                    HtmlGenericControl voteupwrapper = new HtmlGenericControl("div");
                    voteupwrapper.Attributes.Add("class", "vote-button");
                        ImageButton voteupbutton = new ImageButton();
                        voteupbutton.ImageUrl = answer.VotedBy(((User)System.Web.HttpContext.Current.Session["User"]).ID) == 1 ? "Resources/Images/vote-up-checked.png" : "Resources/Images/vote-up-unchecked.png";
                        if(answer.VotedBy(((User)System.Web.HttpContext.Current.Session["User"]).ID) == 1){
                            voteupbutton.Command += new CommandEventHandler(answerUnvoteUp_Click);
                        }
                        else
                        {
                            voteupbutton.Command += new CommandEventHandler(answerVoteUp_Click);
                        }
                        voteupbutton.CommandName = answer.ID.ToString();
                    voteupwrapper.Controls.Add(voteupbutton);

                    HtmlGenericControl acceptwrapper = new HtmlGenericControl("div");
                    acceptwrapper.Attributes.Add("class", "vote-button");
                        ImageButton acceptbutton = new ImageButton();
                        acceptbutton.ImageUrl = answer.Accepted == true ? "Resources/Images/accepted-checked.png" : "Resources/Images/accepted-unchecked.png";
                        if (answer.Accepted)
                        {
                            acceptbutton.Command += new CommandEventHandler(answerUnaccept_Click);
                        }
                        else
                        {
                            acceptbutton.Command += new CommandEventHandler(answerAccept_Click);
                        }
                        acceptbutton.CommandName = answer.ID.ToString();
                    acceptwrapper.Controls.Add(acceptbutton);

                    HtmlGenericControl votedownwrapper = new HtmlGenericControl("div");
                    votedownwrapper.Attributes.Add("class", "vote-button");
                        ImageButton votedownbutton = new ImageButton();
                        votedownbutton.ImageUrl = answer.VotedBy(((User)System.Web.HttpContext.Current.Session["User"]).ID) == -1 ? "Resources/Images/vote-down-checked.png" : "Resources/Images/vote-down-unchecked.png";
                        if (answer.VotedBy(((User)System.Web.HttpContext.Current.Session["User"]).ID) == -1)
                        {
                            votedownbutton.Command += new CommandEventHandler(answerUnvoteDown_Click);
                        }
                        else
                        {
                            votedownbutton.Command += new CommandEventHandler(answerVoteDown_Click);
                        }
                        votedownbutton.CommandName = answer.ID.ToString();
                    votedownwrapper.Controls.Add(votedownbutton);

                votewrapper.Controls.Add(voteupwrapper);
                votewrapper.Controls.Add(acceptwrapper);
                votewrapper.Controls.Add(votedownwrapper);
                
                HtmlGenericControl contentcontainer = new HtmlGenericControl("div");
                contentcontainer.Attributes.Add("class", "content-wrapper");
                    
                    HtmlGenericControl answercontentwrapper = new HtmlGenericControl("div");
                    answercontentwrapper.Attributes.Add("class", "content-wrapper");
                        HtmlGenericControl answercontent = new HtmlGenericControl("span");
                        answercontent.InnerText = answer.Content;
                    answercontentwrapper.Controls.Add(answercontent);
                    
                    HtmlGenericControl answerauthorwrapper = new HtmlGenericControl("div");
                    answerauthorwrapper.Attributes.Add("class", "author-wrapper");
                        HtmlGenericControl author = new HtmlGenericControl("span");
                        author.Attributes.Add("class", "author");
                        author.InnerText = answer.GetAuthor().Username;
                        
                        HtmlGenericControl postdate = new HtmlGenericControl("span");
                        postdate.Attributes.Add("class", "date");
                        postdate.InnerText = answer.PostDate;

                    answerauthorwrapper.Controls.Add(author);
                    answerauthorwrapper.Controls.Add(postdate);

                contentcontainer.Controls.Add(answercontentwrapper);
                contentcontainer.Controls.Add(answerauthorwrapper);

            answercontainer.Controls.Add(votewrapper);
            answercontainer.Controls.Add(contentcontainer);




            return answercontainer;
        }

        protected void answerVoteUp_Click(object sender, CommandEventArgs args)
        {
            int id = Convert.ToInt32(args.CommandName);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().VoteAnswer(userid, id, 1);


            ImageButton button = ((ImageButton)sender);
            button.Command -= new CommandEventHandler(answerVoteUp_Click);
            button.Command += new CommandEventHandler(answerUnvoteUp_Click);
            button.CommandName = id.ToString();

            button.ImageUrl = "Resources/Images/vote-up-checked.png";
        }

        protected void answerAccept_Click(object sender, CommandEventArgs args)
        {
            int id = Convert.ToInt32(args.CommandName);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().AcceptAnswer(id);

            ImageButton button = ((ImageButton)sender);
            button.Command -= new CommandEventHandler(answerAccept_Click);
            button.Command += new CommandEventHandler(answerUnaccept_Click);
            button.CommandName = id.ToString();

            button.ImageUrl = "Resources/Images/accepted-checked.png";
        }

        protected void answerVoteDown_Click(object sender, CommandEventArgs args)
        {
            int id = Convert.ToInt32(args.CommandName);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().VoteAnswer(userid, id, -1);

            ImageButton button = ((ImageButton)sender);
            button.Command -= new CommandEventHandler(answerVoteDown_Click);
            button.Command += new CommandEventHandler(answerUnvoteDown_Click);
            button.CommandName = id.ToString();

            button.ImageUrl = "Resources/Images/vote-down-checked.png";
        }


        protected void answerUnvoteUp_Click(object sender, CommandEventArgs args)
        {
            int id = Convert.ToInt32(args.CommandName);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().UnvoteAnswer(userid, id, 1);

            ImageButton button = ((ImageButton)sender);
            button.Command -= new CommandEventHandler(answerUnvoteUp_Click);
            button.Command += new CommandEventHandler(answerVoteUp_Click);
            button.CommandName = id.ToString();

            button.ImageUrl = "Resources/Images/vote-up-unchecked.png";
        }

        protected void answerUnaccept_Click(object sender, CommandEventArgs args)
        {
            int id = Convert.ToInt32(args.CommandName);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().UnacceptAnswer(id);

            ImageButton button = ((ImageButton)sender);
            button.Command -= new CommandEventHandler(answerUnaccept_Click);
            button.Command += new CommandEventHandler(answerAccept_Click);
            button.CommandName = id.ToString();

            button.ImageUrl = "Resources/Images/accepted-unchecked.png";
        }

        protected void answerUnvoteDown_Click(object sender, CommandEventArgs args)
        {
            int id = Convert.ToInt32(args.CommandName);
            int userid = (((User)System.Web.HttpContext.Current.Session["User"]).ID);
            DatabaseHandler.GetInstance().UnvoteAnswer(userid, id, -1);

            ImageButton button = ((ImageButton)sender);
            button.Command -= new CommandEventHandler(answerUnvoteDown_Click);
            button.Command += new CommandEventHandler(answerVoteDown_Click);
            button.CommandName = id.ToString();

            button.ImageUrl = "Resources/Images/vote-down-unchecked.png";
        }

    }
}