﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Oracle.DataAccess;
using Oracle.DataAccess.Client;

namespace Stackoverflow
{
    public class DatabaseHandler
    {

        private static DatabaseHandler self;

        private OracleConnection con;
        private OracleCommand cmd;
        private OracleDataReader dr;
        private OracleTransaction tran;

        private DatabaseHandler()
        {
            self = this;
            //Connect();
            //Disconnect();
        }

        public static DatabaseHandler GetInstance()
        {
            if (self == null)
            {
                return new DatabaseHandler();
            }
            else
            {
                return self;
            }
        }

        private void Connect()
        {
            con = new OracleConnection();
            //con.ConnectionString = "User ID=dbi317913; Password=I8zOKDgbJd; Data Source=192.168.15.50:1521/fhictora;";
            con.ConnectionString = "User Id=Stackoverflow;Password=drowssap;Pooling=false;Data Source=localhost/xe;";
            con.Open();
            Console.WriteLine("CONNECTION SUCCESFULL");

        }
        
        private void Disconnect()
        {
            con.Close();
            con.Dispose();
        }

        private void ReadData(string sql)
        {
            try
            {
                cmd = new OracleCommand();
                cmd.Connection = con;
                cmd.CommandText = sql;
                cmd.CommandType = System.Data.CommandType.Text;

                dr = cmd.ExecuteReader();
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.ToString());
            }
        }

        private void WriteData(string sql)
        {
            try
            {
                cmd = new OracleCommand();
                tran = con.BeginTransaction();
                tran.Commit();
                cmd.Transaction = tran;

                cmd.Connection = con;
                cmd.CommandText = sql;
                cmd.ExecuteNonQuery();
                
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }

        public int GetNextID(string columnname, string idname)
        {
            Connect();
            ReadData("SELECT MAX(" + idname + ") FROM " + columnname);
            try
            {
                while (dr.Read())
                {

                    int id = dr.GetInt32(0);

                    return id+1;

                }
            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            return 0;
        }


        public List<Message> downloadMessages(int userid)
        {
            Connect();
            List<Message> messagelist = new List<Message>();

            ReadData("SELECT * FROM SO_INBOXMESSAGE WHERE INBOXID = " + userid);

            try
            {
                while (dr.Read())
                {
                    int messageid;
                    int inboxid;
                    DateTime messagedate;
                    string messagecontent;

                    messageid = dr.GetInt32(0);
                    inboxid = dr.GetInt32(1);
                    messagedate = dr.GetDateTime(2);
                    messagecontent = dr.GetString(3);

                    messagelist.Add(new Message(messageid, inboxid, messagedate, messagecontent));
                }

                Disconnect();
                return messagelist;
            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            Disconnect();
            return null;
        }

        /// <summary>
        /// Downloads all questions.
        /// </summary>
        /// <param name="amount">The amount of questions to download.</param>
        /// <returns>A list of questions</returns>
        public List<Question> DownloadQuestions(int amount = 10)
        {
            Connect();
            List<Question> questionlist = new List<Question>();

            string query = amount == 0 ? "SELECT * FROM SO_QUESTION" : "SELECT * FROM SO_QUESTION WHERE ROWNUM <= " + amount;

            ReadData(query);
            try
            {
                while (dr.Read())
                {
                    int questionid;
                    int userid;
                    string title;
                    string content;
                    int upvotes;
                    int downvotes;
                    int favorites;
                    string date;

                    questionid = dr.GetInt32(0);
                    userid = dr.GetInt32(1);
                    content = dr.GetString(2);
                    upvotes = dr.GetInt32(3);
                    downvotes = dr.GetInt32(4);
                    favorites = dr.GetInt32(5);
                    title = dr.GetString(6);
                    date = dr.GetString(7);

                    questionlist.Add(new Question(questionid, userid, title, content, upvotes, downvotes, favorites, date));
                    
                }
                Disconnect();
                return questionlist;
            }
            catch (InvalidCastException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
            Disconnect();
            return null;
        }

        public List<Question> DownloadQuestionsByTag(string tag)
        {
            Connect();

            List<Question> questionlist = new List<Question>();

            ReadData("SELECT * FROM SO_QUESTION WHERE QUESTIONID IN (SELECT QUESTIONID FROM SO_QUESTION_TAG WHERE TAGID = (SELECT TAGID FROM SO_TAG WHERE TAGNAME = '"+tag+"'))");
            
            try
            {
                while (dr.Read())
                {
                    int questionid;
                    int userid;
                    string title;
                    string content;
                    int upvotes;
                    int downvotes;
                    int favorites;
                    string date;

                    questionid = dr.GetInt32(0);
                    userid = dr.GetInt32(1);
                    content = dr.GetString(2);
                    upvotes = dr.GetInt32(3);
                    downvotes = dr.GetInt32(4);
                    favorites = dr.GetInt32(5);
                    title = dr.GetString(6);
                    date = dr.GetString(7);

                    questionlist.Add(new Question(questionid, userid, title, content, upvotes, downvotes, favorites, date));

                }
                Disconnect();
                return questionlist;
            }
            catch (InvalidCastException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
            Disconnect();
            return questionlist;
        }

        /// <summary>
        /// Downloads the question by identifier.
        /// </summary>
        /// <param name="questionid">The questionid.</param>
        /// <returns>A new question object</returns>
        public Question DownloadQuestionByID(int questionid)
        {
            Connect();
            ReadData("SELECT * FROM SO_QUESTION WHERE QUESTIONID = " + questionid);
            try
            {
                while (dr.Read())
                {
                    int userid;
                    string title;
                    string content;
                    int upvotes;
                    int downvotes;
                    int favorites;
                    string date;

                    userid = dr.GetInt32(1);
                    content = dr.GetString(2);
                    upvotes = dr.GetInt32(3);
                    downvotes = dr.GetInt32(4);
                    favorites = dr.GetInt32(5);
                    title = dr.GetString(6);
                    date = dr.GetString(7);

                    Disconnect();
                    return new Question(questionid, userid, title, content, upvotes, downvotes, favorites, date);

                }
            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            Disconnect();
            return null;
        }

        public List<Answer> DownloadAnswersForQuestion(Question question)
        {
            Connect();
            List<Answer> answerlist = new List<Answer>();

            ReadData("SELECT * FROM SO_ANSWER WHERE QUESTIONID = " + question.ID);
            try
            {
                while (dr.Read())
                {
                    int answerid;
                    int userid;
                    string content;
                    int upvotes;
                    int downvotes;
                    bool accepted;
                    string date;

                    answerid = dr.GetInt32(0);
                    userid = dr.GetInt32(2);
                    content = dr.GetString(3);
                    upvotes = dr.GetInt32(4);
                    downvotes = dr.GetInt32(5);
                    accepted = dr.GetInt32(6) == 1 ? true : false;
                    date = dr.GetString(7);

                    answerlist.Add(new Answer(answerid, userid, question, content, upvotes, downvotes, accepted, date));

                }

                Disconnect();
                return answerlist;
            }
            catch (InvalidCastException ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.ToString());
            }
            Disconnect();
            return answerlist;
        }

        public int CheckQuestionVoteByUser(int questionid, int userid)
        {
            Connect();
            ReadData("SELECT * FROM SO_USER_VOTE WHERE QUESTIONID = " + questionid + " AND USERID = " + userid);
            try
            {
                while (dr.Read())
                {
                    int vote;

                    vote = dr.GetInt32(3);

                    Disconnect();
                    return vote;

                }
            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            Disconnect();
            return 0;
        }

        public int CheckAnswerVoteByUser(int answerid, int userid)
        {
            Connect();
            ReadData("SELECT * FROM SO_USER_VOTE WHERE ANSWERID = " + answerid + " AND USERID = " + userid);
            try
            {
                while (dr.Read())
                {
                    int vote;

                    vote = dr.GetInt32(3);

                    Disconnect();
                    return vote;

                }
            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            Disconnect();
            return 0;
        }


        /// <summary>
        /// Checks if the question is favorited by current user.
        /// </summary>
        /// <param name="questionid">The questionid.</param>
        /// <param name="userid">The userid.</param>
        /// <returns>True if the selected question is favorited by the given userid</returns>
        public bool CheckQuestionFavoritedByUser(int questionid, int userid)
        {
            Connect();
            ReadData("SELECT * FROM SO_USER_FAVORITES WHERE QUESTIONID = " + questionid + " AND USERID = " + userid);
            try
            {
                while (dr.Read())
                {
                    Disconnect();
                    return true;

                }
            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            Disconnect();
            return false;
        }
       
        public bool ValidateUser(string username, string password)
        {
            Connect();
            cmd = new OracleCommand();
            cmd.Connection = con;
            cmd.CommandText = "SELECT * FROM SO_USER_CREDENTIALS WHERE USERNAME = :userName";
            cmd.Parameters.Add("userName", OracleDbType.Varchar2).Value = username;
            cmd.CommandType = System.Data.CommandType.Text;
            dr = cmd.ExecuteReader();

            try
            {
                while (dr.Read())
                {
                    int userid;

                    userid = dr.GetInt32(0);
                    if (username == dr.GetString(1) && password == dr.GetString(2))
                    {
                        Disconnect();
                        return true;
                    }
                    Disconnect();
                    return false;
                }

            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            Disconnect();
            return false;
        }

        public User DownloadUser(int userid)
        {
            Connect();
            ReadData("SELECT * FROM SO_USER WHERE USERID = " + userid);
            try
            {
                while (dr.Read())
                {
                    int inboxid;
                    int achievementinboxid;
                    string username;
                    int reputation;
                    string realname;
                    string userlocation;
                    int age;
                    int memberfor;
                    int visited;
                    string lastseen;
                    int profileviews;
                    string email;

                    inboxid = dr.GetInt32(1);
                    achievementinboxid = dr.GetInt32(2);
                    username = dr.GetString(3);
                    reputation = dr.GetInt32(4);
                    realname = dr.GetString(5);
                    userlocation = dr.GetString(6);
                    age = dr.GetInt32(7);
                    memberfor = dr.GetInt32(8);
                    visited = dr.GetInt32(9);
                    lastseen = dr.GetString(10);
                    profileviews = dr.GetInt32(11);
                    email = dr.GetString(12);

                    Disconnect();
                    return new User(userid, username, reputation, memberfor, visited, lastseen, profileviews);
                }

            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            Disconnect();
            return null;
        }

        public User DownloadUser(string username)
        {
            Connect();
            ReadData("SELECT * FROM SO_USER WHERE USERNAME = '" + username + "'");
            try
            {
                while (dr.Read())
                {
                    int userid;
                    int inboxid;
                    int achievementinboxid;
                    int reputation;
                    string realname;
                    string userlocation;
                    int age;
                    int memberfor;
                    int visited;
                    string lastseen;
                    int profileviews;
                    string email;

                    userid = dr.GetInt32(0);
                    inboxid = dr.GetInt32(1);
                    achievementinboxid = dr.GetInt32(2);
                    reputation = dr.GetInt32(4);
                    realname = dr.GetString(5);
                    userlocation = dr.GetString(6);
                    age = dr.GetInt32(7);
                    memberfor = dr.GetInt32(8);
                    visited = dr.GetInt32(9);
                    lastseen = dr.GetString(10);
                    profileviews = dr.GetInt32(11);
                    email = dr.GetString(12);

                    Disconnect();
                    return new User(userid, username, reputation, memberfor, visited, lastseen, profileviews);
                }                
            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            Disconnect();
            return null;
        }

        public List<string> DownloadBadgesForUser(int userid)
        {
            Connect();
            List<string> badgelist = new List<string>();

            ReadData("SELECT BADGENAME FROM SO_BADGE WHERE BADGEID IN (SELECT DISTINCT BADGEID FROM SO_USER_BADGE WHERE USERID =" + userid + ")");
            try
            {
                while (dr.Read())
                {
                    badgelist.Add(dr.GetString(0));
                }
                Disconnect();
                return badgelist;
            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            Disconnect();
            return null;
        }

        public List<string> DownloadTagsForQuestion(int questionid)
        {
            Connect();
            List<string> taglist = new List<string>();

            if (questionid == 0)
            {
                ReadData("SELECT TAGNAME FROM SO_TAG");
                try
                {
                    while (dr.Read())
                    {
                        taglist.Add(dr.GetString(0));
                    }
                    Disconnect();
                    return taglist;
                }
                catch (InvalidCastException ex)
                {
                    //MessageBox.Show(ex.ToString());
                }
            }
            else
            {
                ReadData("SELECT TAGNAME FROM SO_TAG WHERE TAGID IN (SELECT DISTINCT TAGID FROM SO_QUESTION_TAG WHERE QUESTIONID = " + questionid + ")");
                try
                {
                    while (dr.Read())
                    {
                        taglist.Add(dr.GetString(0));
                    }
                    Disconnect();
                    return taglist;
                }
                catch (InvalidCastException ex)
                {
                    //MessageBox.Show(ex.ToString());
                }
            }
            Disconnect();
            return null;
        }


        // UPLOADS
        public void UploadNewQuestion(Question question)
        {
            Connect();

            // Add the question to the database
            WriteData(String.Format("INSERT INTO SO_QUESTION (QUESTIONID, USERID, QUESTIONCONTENT, UPVOTE, DOWNVOTE, FAVORITES, TITLE, POSTDATE) VALUES ({0}, {1}, '{2}', {3}, {4}, {5}, '{6}', '{7}')",
                question.ID, question.UserID, question.Content, question.Upvotes, question.Downvotes, question.Favorites, question.Title, question.PostDate));
           

            // Retrieve all existing tags
            ReadData("SELECT * FROM SO_TAG");
            List<KeyValuePair<int, string>> taglist = new List<KeyValuePair<int,string>>();
            try
            {
                while (dr.Read())
                {

                    int id = dr.GetInt32(0);
                    string name = dr.GetString(1);

                    taglist.Add(new KeyValuePair<int,string>(id,name));
                }
            }
            catch (InvalidCastException ex)
            {
                //MessageBox.Show(ex.ToString());
            }

            // Loop through given tags
            foreach(string addtag in question.TagList){
               
                // True if the tag doesn't exist yet
                bool newtag = true;
                
                // Check if the tag exists
                foreach (KeyValuePair<int, string> tag in taglist)
                {
                    if (addtag.Equals(tag.Value))
                    {
                        newtag = false;
                        WriteData(String.Format("INSERT INTO SO_QUESTION_TAG (QUESTIONID, TAGID) VALUES({0}, {1})", question.ID, tag.Key));
                        break;
                    }
                }

                // Add the newly created tag
                if (newtag)
                {
                    int id = GetNextID("SO_TAG", "TAGID");
                    WriteData(String.Format("INSERT INTO SO_TAG (TAGID, TAGNAME) VALUES ({0}, '{1}')", id, addtag));
                    WriteData(String.Format("INSERT INTO SO_QUESTION_TAG (QUESTIONID, TAGID) VALUES ({0}, {1})", question.ID, id));
                }

            }
            Disconnect();
        }

        public void UploadNewAnswer(Answer answer, Question question)
        {
            Connect();

            // Add the question to the database
            WriteData(String.Format("INSERT INTO SO_ANSWER (ANSWERID, QUESTIONID, USERID, ANSWERCONTENT, UPVOTE, DOWNVOTE, ACCEPTED, POSTDATE) VALUES ({0}, {1}, {2}, '{3}', {4}, {5}, {6}, '{7}')",
                answer.ID, question.ID, answer.UserID, answer.Content, answer.Upvotes, answer.Downvotes, (answer.Accepted == true ? 1 : 0), answer.PostDate));
           
            Disconnect();
        }

        /// <summary>
        /// Vote the answer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="vote">The vote. 1=upvote -1=downvote </param>
        public void VoteAnswer(int userid, int answerid, int vote)
        {
            Connect();
            switch(vote){
                case 1:
                    WriteData(String.Format("UPDATE SO_ANSWER SET UPVOTE = (SELECT UPVOTE FROM SO_ANSWER WHERE ANSWERID = {0})+1 WHERE ANSWERID = {0}", answerid));
                    WriteData(String.Format("INSERT INTO SO_USER_VOTE (USERID, QUESTIONID, ANSWERID, VOTE) VALUES ({0},NULL,{1},1)", userid, answerid));

                    break;
                case -1:
                    WriteData(String.Format("UPDATE SO_ANSWER SET DOWNVOTE = (SELECT DOWNVOTE FROM SO_ANSWER WHERE ANSWERID = {0})+1 WHERE ANSWERID = {0}", answerid));
                    WriteData(String.Format("INSERT INTO SO_USER_VOTE (USERID, QUESTIONID, ANSWERID, VOTE) VALUES ({0},NULL,{1},-1)", userid, answerid));

                    break;
            }
            Disconnect();
        }

        public void UnvoteAnswer(int userid, int answerid, int vote)
        {
            Connect();
            switch (vote)
            {
                case 1:
                    WriteData(String.Format("UPDATE SO_ANSWER SET UPVOTE = (SELECT UPVOTE FROM SO_ANSWER WHERE ANSWERID = {0})-1 WHERE ANSWERID = {0}", answerid));
                    WriteData(String.Format("DELETE FROM SO_USER_VOTE WHERE USERID  = {0} AND ANSWERID = {1}", userid, answerid));

                    break;
                case -1:
                    WriteData(String.Format("UPDATE SO_ANSWER SET DOWNVOTE = (SELECT DOWNVOTE FROM SO_ANSWER WHERE ANSWERID = {0})-1 WHERE ANSWERID = {0}", answerid));
                    WriteData(String.Format("DELETE FROM SO_USER_VOTE WHERE USERID  = {0} AND ANSWERID = {1}", userid, answerid));

                    break;
            }
            Disconnect();
        }

        public void AcceptAnswer(int answerid)
        {
            Connect();
            WriteData(String.Format("UPDATE SO_ANSWER SET ACCEPTED = 1 WHERE ANSWERID = {0}", answerid));
            Disconnect();
        }

        public void UnacceptAnswer(int answerid)
        {
            Connect();
            WriteData(String.Format("UPDATE SO_ANSWER SET ACCEPTED = 0 WHERE ANSWERID = {0}", answerid));
            Disconnect();
        }

        public void VoteQuestion(int userid, int questionid, int vote)
        {
            Connect();
            switch (vote)
            {
                case 1:
                    WriteData(String.Format("UPDATE SO_QUESTION SET UPVOTE = (SELECT UPVOTE FROM SO_QUESTION WHERE QUESTIONID = {0})+1 WHERE QUESTIONID = {0}", questionid));
                    WriteData(String.Format("INSERT INTO SO_USER_VOTE (USERID, QUESTIONID, ANSWERID, VOTE) VALUES ({0},{1}, NULL,1)", userid, questionid));

                    break;
                case -1:
                    WriteData(String.Format("UPDATE SO_QUESTION SET DOWNVOTE = (SELECT DOWNVOTE FROM SO_QUESTION WHERE QUESTIONID = {0})+1 WHERE QUESTIONID = {0}", questionid));
                    WriteData(String.Format("INSERT INTO SO_USER_VOTE (USERID, QUESTIONID, ANSWERID, VOTE) VALUES ({0},{1}, NULL,-1)", userid, questionid));

                    break;
            }
            Disconnect();
        }

        public void UnvoteQuestion(int userid, int questionid, int vote)
        {
            Connect();
            switch (vote)
            {
                case 1:
                    WriteData(String.Format("UPDATE SO_QUESTION SET UPVOTE = (SELECT UPVOTE FROM SO_QUESTION WHERE QUESTIONID = {0})-1 WHERE QUESTIONID = {0}", questionid));
                    WriteData(String.Format("DELETE FROM SO_USER_VOTE WHERE USERID  = {0} AND QUESTIONID = {1}", userid, questionid));

                    break;
                case -1:
                    WriteData(String.Format("UPDATE SO_QUESTION SET DOWNVOTE = (SELECT DOWNVOTE FROM SO_QUESTION WHERE QUESTIONID = {0})-1 WHERE QUESTIONID = {0}", questionid));
                    WriteData(String.Format("DELETE FROM SO_USER_VOTE WHERE USERID  = {0} AND QUESTIONID = {1}", userid, questionid));

                    break;
            }
            Disconnect();
        }

        public void FavoriteQuestion(int questionid)
        {
            Connect();
            WriteData(String.Format("UPDATE SO_QUESTION SET FAVORITED = 1 WHERE QUESTIONID = {0}", questionid));
            Disconnect();
        }

        public void UnfavoriteQuestion(int questionid)
        {
            Connect();
            WriteData(String.Format("UPDATE SO_QUESTION SET FAVORITED = 0 WHERE QUESTIONID = {0}", questionid));
            Disconnect();
        }
    }
}