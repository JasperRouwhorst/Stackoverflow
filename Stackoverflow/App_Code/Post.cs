﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stackoverflow
{
    public class Post
    {

        public readonly int ID;
        public readonly int UserID;

        
        public string Content { get; set; }
        public string PostDate { get; set; }
        public int Upvotes { get; set; }
        public int Downvotes { get; set; }

        public Post(int id, int userid, string content, int upvotes, int downvotes, string postdate)
        {
            ID = id;
            UserID = userid;
            Content = content;
            Upvotes = upvotes;
            Downvotes = downvotes;
            PostDate = postdate;
        }

        public User GetAuthor()
        {
            return DatabaseHandler.GetInstance().DownloadUser(UserID);
        }

        


    }
}