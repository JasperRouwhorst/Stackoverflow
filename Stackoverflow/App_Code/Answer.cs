﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stackoverflow
{
    public class Answer : Post
    {

        public bool Accepted { get; set; }

        Question parentquestion;

        public Answer(int id, int userid, Question question, string content, int upvotes, int downvotes, string postdate)
            : base(id, userid, content, upvotes, downvotes, postdate)
        {
            parentquestion = question;
        }

        public Answer(int id, int userid, Question question, string content, int upvotes, int downvotes, bool accepted, string postdate)
            : base(id, userid, content, upvotes, downvotes, postdate)
        {
            parentquestion = question;
            Accepted = accepted;
        }

        public int VotedBy(int userid)
        {
            return DatabaseHandler.GetInstance().CheckAnswerVoteByUser(ID, userid);
        }

    }
}