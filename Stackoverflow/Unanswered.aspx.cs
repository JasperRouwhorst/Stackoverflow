﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Stackoverflow
{
    public partial class Unanswered : System.Web.UI.Page
    {

        List<Question> questionList;

        protected void Page_Load(object sender, EventArgs e)
        {
            questionList = DatabaseHandler.GetInstance().DownloadQuestions(0);

            foreach (Question question in questionList)
            {
                if (!question.Answered)
                {
                    UnansweredDiv.Controls.Add(WindowHandler.GetInstance().DrawQuestionListItem(question));
                }
            }
        }
    }
}