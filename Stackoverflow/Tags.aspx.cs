﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Stackoverflow
{
    public partial class Tags : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> taglist = DatabaseHandler.GetInstance().DownloadTagsForQuestion(0);

            foreach (string tag in taglist)
            {
                TagsPage.InnerHtml += "<a href=\"Questions.aspx?tag="+tag+"\"><div class=\"tag\">"+tag+"</div></a>";
            }
        }
    }
}