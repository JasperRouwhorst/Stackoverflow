﻿<%@ Page Language="C#" MasterPageFile="~/Stackoverflow.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Stackoverflow.Index" %>

<asp:Content ID="IndexHead" ContentPlaceHolderID="HeadPlaceHolder" runat="server">
    <link href="Resources/Styles/index-style.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="IndexContent" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <ul id="questionList" runat="server">
            <!-- Dynamic content --> 
    </ul>
</asp:Content>
