﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stackoverflow;
namespace StackoverflowUnitTest
{
    [TestClass]
    public class UserTest
    {
        [TestMethod]
        public void TestUsers()
        {
            User user = new User(1, "Jasper");
            user.Age = 22;
            user.Email = "jasper.rouwhorst93@gmail.com";
            user.Reputation = 132;

            Assert.AreEqual("Jasper", user.Username, "Username incorrect");
            Assert.AreEqual(22, user.Age, "Age incorrect");
            Assert.AreEqual("jasper.rouwhorst93@gmail.com", user.Email, "Email incorrect");
            Assert.AreEqual(132, user.Reputation, "Reputation incorrect");
        }

        [TestMethod]
        public void TestQuestion()
        {
            Question question = new Question(1, 11, "Testtitle", "Testcontent", 0, 0, 0, "12-06-2015");
            Assert.AreEqual(1, question.ID, "ID incorrect");
            Assert.AreEqual(11, question.UserID, "UserID incorrect");
            Assert.AreEqual("Testtitle", question.Title, "Title incorrect");
            Assert.AreEqual("Testcontent", question.Content, "Content incorrect");
            Assert.AreEqual(0, question.Upvotes, "Upvotes incorrect");
            Assert.AreEqual(0, question.Downvotes, "Downvotes incorrect");
            Assert.AreEqual(0, question.Favorites, "Favorites incorrect");
            Assert.AreEqual("12-06-2015", question.PostDate, "PostDate incorrect");
        }

        [TestMethod]
        public void TestAnswer()
        {
            Question question = new Question(1, 11, "Testtitle", "Testcontent", 0, 0, 0, "12-06-2015");
            Answer answer = new Answer(1, 11, question, "Testcontent", 0, 0, false, "12-06-2015");

            Assert.AreEqual(1, answer.ID, "ID incorrect");
            Assert.AreEqual(11, answer.UserID, "UserID incorrect");
            Assert.AreEqual("Testcontent", answer.Content, "Content incorrect");
            Assert.AreEqual(0, answer.Upvotes, "Upvotes incorrect");
            Assert.AreEqual(0, answer.Downvotes, "Downvotes incorrect");
            Assert.AreEqual(false, answer.Accepted, "Accepted incorrect");
            Assert.AreEqual("12-06-2015", answer.PostDate, "PostDate incorrect");
        }
    }
}
